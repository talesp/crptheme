# CRPTheme

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CRPTheme is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "CRPTheme"

## Author

Tales Pinheiro de Andrade, tales@newtlabs.com

## License

CRPTheme is available under the MIT license. See the LICENSE file for more info.

