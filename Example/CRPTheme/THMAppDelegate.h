//
//  THMAppDelegate.h
//  CRPTheme
//
//  Created by CocoaPods on 02/24/2015.
//  Copyright (c) 2014 Tales Pinheiro de Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
