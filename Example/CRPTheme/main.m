//
//  main.m
//  CRPTheme
//
//  Created by Tales Pinheiro de Andrade on 02/24/2015.
//  Copyright (c) 2014 Tales Pinheiro de Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "THMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([THMAppDelegate class]));
    }
}
